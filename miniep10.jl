function submatrix(m, sz, a, b)
	sum = 0
	for i in a:a+sz-1
		for j in b:b+sz-1
			sum += m[i, j]
		end
	end
	return sum
end

#deve funcionar
function max_sum_submatrix(m)
	maxΣ = -Inf
	result = []
	for sz in 1:size(m)[1]
		for i in 1:size(m)[1] - sz + 1
			for j in 1:size(m)[2] - sz + 1
				subm = submatrix(m, sz, i, j)
                subm == maxΣ && push!(result, m[i:(i + sz - 1), j:(j + sz -1)])
                #fast path
                if subm > maxΣ
					maxΣ = subm
                    result = [ m[i:(i + sz - 1), j:(j + sz -1)] ]
                end
			end
		end
	end
	return result
end



using Test

function test()
    @test max_sum_submatrix([-1 1 1; 1 -1 1; 1 1 -2]) == [[1 1; -1 1], [1 -1; 1 1], [-1 1 1; 1 -1 1; 1 1 -2]]
	@test max_sum_submatrix([1 1 1; 1 1 1; 1 1 1]) == [[1 1 1; 1 1 1; 1 1 1]]
	@test max_sum_submatrix([0 -1; -2 -3]) == [hcat(0)] #hcat remove erro de [[0]] == [[0]]
	println("testes feitos com sucesso")
end

#test()



